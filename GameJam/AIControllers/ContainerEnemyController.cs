﻿using GameJam.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameJam.AIControllers
{
	class ContainerEnemyController : ActorController
	{
		Vehicle[] turrets;
		Vector2 targetPos;
		int spacing = 50;
		public ContainerEnemyController(Vehicle vehicle, Vector2 targetPos, Vehicle turret1) : base(vehicle)
		{
			this.targetPos = targetPos;
			turret1.Position = new Vector2(vehicle.Position.X, vehicle.Position.Y + spacing);
			turrets = new Vehicle[] { turret1 };
		}
		public ContainerEnemyController(Vehicle vehicle, Vector2 targetPos, Vehicle turret1, Vehicle turret2) : base(vehicle)
        {
			this.targetPos = targetPos;
			turret1.Position = new Vector2(vehicle.Position.X, vehicle.Position.Y + spacing);
			turret2.Position = new Vector2(turret1.Position.X, turret1.Position.Y + spacing);

			turrets = new Vehicle[] {turret1, turret2};
		}

		public ContainerEnemyController(Vehicle vehicle, Vector2 targetPos, Vehicle turret1, Vehicle turret2, Vehicle turret3) : base(vehicle)
		{
			this.targetPos = targetPos;
			turret1.Position = new Vector2(vehicle.Position.X, vehicle.Position.Y + spacing);
			turret2.Position = new Vector2(turret1.Position.X, turret1.Position.Y + spacing);
			turret3.Position = new Vector2(turret2.Position.X, turret2.Position.Y + spacing);

			turrets = new Vehicle[] { turret1, turret2, turret3 };
		}
		public override void UpdateController(GameTime gameTime)
		{
			if (vehicle.Position.X > targetPos.X + 10)
			{
				vehicle.MoveLeft(gameTime);
				for (int i = 0; i< turrets.Count(); i++)
				{
					turrets[i].MoveLeft(gameTime);
				}
			}
			else if (vehicle.Position.X < targetPos.X + 10)
			{
				vehicle.MoveRight(gameTime);
				for (int i = 0; i < turrets.Count(); i++)
				{
					turrets[i].MoveRight(gameTime);
				}
			}

			if (vehicle.Position.Y > targetPos.Y + 10)
			{
				this.vehicle.MoveUp(gameTime);
				for (int i = 0; i < turrets.Count(); i++)
				{
					turrets[i].MoveUp(gameTime);
				}
			}
			else if (vehicle.Position.Y < targetPos.Y + 10)
			{
				vehicle.MoveDown(gameTime);
				for (int i = 0; i < turrets.Count(); i++)
				{
					turrets[i].MoveDown(gameTime);
				}
			}

			for (int i = 0; i < turrets.Count(); i++)
			{
				turrets[i].controller.UpdateController(gameTime);
			}
		}
	}
}
