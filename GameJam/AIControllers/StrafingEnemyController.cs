﻿using GameJam.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace GameJam
{
	class StrafingEnemyController : ActorController
	{
		float fireRate;
		float timeSinceLastFired;
		private Vector2 endPos;
        public StrafingEnemyController(Vehicle vehicle,float fireRate, Vector2 endPos) : base(vehicle)
        {
			this.fireRate = fireRate;
			this.endPos = endPos;
		}

		public override void UpdateController(GameTime gameTime)
		{
			if (vehicle.Position.X > endPos.X + 10)
			{
				vehicle.MoveLeft(gameTime);
			}
			else if (vehicle.Position.X < endPos.X + 10)
			{
				vehicle.MoveRight(gameTime);
			}

			if (vehicle.Position.Y > endPos.Y + 10)
			{
				this.vehicle.MoveUp(gameTime);
			}
			else if (vehicle.Position.Y < endPos.Y + 10)
			{
				vehicle.MoveDown(gameTime);
			}


			if (fireRate < timeSinceLastFired + gameTime.ElapsedGameTime.Milliseconds)
			{
				timeSinceLastFired = 0;
				vehicle.primary.Fire(this.vehicle.Position, new Vector2(0, 10));
			}
			else
			{
				timeSinceLastFired += gameTime.ElapsedGameTime.Milliseconds;
            }

			if (Vector2.Distance(vehicle.Position,endPos) <= 10)
			{
				//to do: despawn vehical.
			}
		}
	}
}

