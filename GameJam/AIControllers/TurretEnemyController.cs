﻿using GameJam.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace GameJam.AIControllers
{
	class TurretEnemyController : ActorController
	{
		Vehicle target;
		float fireRate;
		float timeSinceLastFired;
		public TurretEnemyController(Vehicle vehicle, float fireRate, Vehicle target) : base(vehicle)
        {
			this.fireRate = fireRate;
			this.target = target;
		}
		public override void UpdateController(GameTime gameTime)
		{
			if (fireRate < timeSinceLastFired + gameTime.ElapsedGameTime.Milliseconds)
			{
				timeSinceLastFired = 0;

				Vector2 temp = target.Position - this.vehicle.Position;
				temp.Normalize();

				vehicle.primary.Fire(this.vehicle.Position, temp);

				// note will have to be in the direction of the player
			}
			else
			{
				timeSinceLastFired += gameTime.ElapsedGameTime.Milliseconds;
			}
		}
	}
}
