﻿using GameJam.Input;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameJam.AIControllers
{
	class TrackingEnemyController : ActorController
	{
		float fireRate;
		float timeSinceLastFired;
		Vehicle target;
        public TrackingEnemyController(Vehicle vehicle, float fireRate, Vehicle target) : base(vehicle)
		{
			this.fireRate = fireRate;
			this.target = target;
		}

		public override void UpdateController(GameTime gameTime)
		{
			if (vehicle.Position.X > target.Position.X + 10)
			{
				vehicle.MoveLeft(gameTime);
			}
			else if (vehicle.Position.X < target.Position.X + 10)
			{
				vehicle.MoveRight(gameTime);
			}

			if (fireRate < timeSinceLastFired + gameTime.ElapsedGameTime.Milliseconds && (vehicle.Position.X - target.Position.X <= 10 && vehicle.Position.X - target.Position.X >= -10))
			{
				timeSinceLastFired = 0;
				vehicle.primary.Fire(this.vehicle.Position, new Vector2(0, 10));
			}
			else
			{
				timeSinceLastFired += gameTime.ElapsedGameTime.Milliseconds;
			}
		}
	}
}
