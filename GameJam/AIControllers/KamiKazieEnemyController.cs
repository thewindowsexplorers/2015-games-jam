﻿using GameJam.Input;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameJam.AIControllers
{
	class KamiKazieEnemyController : ActorController
	{
		Vehicle target;
		public KamiKazieEnemyController(Vehicle vehicle, Vehicle target) : base(vehicle)
        {
			this.target = target;
		}

		public override void UpdateController(GameTime gameTime)
		{
			if (vehicle.Position.X > target.Position.X + 10)
			{
				vehicle.MoveLeft(gameTime);
			}
			else if (vehicle.Position.X < target.Position.X + 10)
			{
				vehicle.MoveRight(gameTime);
			}

			if (vehicle.Position.Y > target.Position.Y + 10)
			{
				this.vehicle.MoveUp(gameTime);
			}
			else if (vehicle.Position.Y < target.Position.Y + 10)
			{
				vehicle.MoveDown(gameTime);
			}

			// to do collision check then explode
		}
	}
}
