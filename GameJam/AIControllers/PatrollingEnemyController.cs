﻿using GameJam.Input;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameJam.AIControllers
{
	class PatrollingEnemyController : ActorController
	{
		float fireRate;
		float timeSinceLastFired;
		Vector2[] points;
		int nextIndex = 0;
		Vector2 endPos;
		bool finalPoint = false;
		bool circular = false;

        public PatrollingEnemyController(Vehicle vehicle, float fireRate, Vector2[] points, Vector2 endPos) : base(vehicle)
		{
			this.fireRate = fireRate;
			this.points = points;
			this.endPos = endPos;
		}

		public PatrollingEnemyController(Vehicle vehicle, float fireRate, Vector2[] points) : base(vehicle)
		{
			this.fireRate = fireRate;
			this.points = points;
			circular = true;
		}

		public override void UpdateController(GameTime gameTime)
		{
			Vector2 temp;
            if (finalPoint)
			{
				if (circular)
				{
					temp = points[0];
					finalPoint = false;
					nextIndex = 0;
                }
				else
				{
					temp = endPos;
				}
			}
			else
			{
				 temp = points[nextIndex];
			}

            if (vehicle.Position.X > temp.X + 10)
			{
				vehicle.MoveLeft(gameTime);
			}
			else if (vehicle.Position.X < temp.X + 10)
			{
				vehicle.MoveRight(gameTime);
			}

			if (vehicle.Position.Y > temp.Y + 10)
			{
				this.vehicle.MoveUp(gameTime);
			}
			else if (vehicle.Position.Y < temp.Y + 10)
			{
				vehicle.MoveDown(gameTime);
			}

			if (Vector2.Distance(vehicle.Position, temp) <= 10)
			{
				if (nextIndex == points.Count())
				{
					if (temp == endPos)
					{
						//to do: despawn vehical.
					}
					finalPoint = true;
				}
				else
				{
					nextIndex++;
					if (nextIndex == points.Count())
					{
						finalPoint = true;
					}
				}
			}


			if (fireRate < timeSinceLastFired + gameTime.ElapsedGameTime.Milliseconds)
			{
				timeSinceLastFired = 0;
				vehicle.primary.Fire(this.vehicle.Position,new Vector2(0,10));
			}
			else
			{
				timeSinceLastFired += gameTime.ElapsedGameTime.Milliseconds;
			}
		}
	}
}
