﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameJam
{
    public class GameObject
    {
        public Vector2 Position { get; set; }
        public Vector2 Velocity { get; set; }
        public float Rotation { get; set; }
        public float Scale { get; set; }
		public Rectangle CollisionBoundary { get; set; }

		private Sprite m_sprite;
		// bounding box of sprite...used for collisions
		private Rectangle m_boundingBox;
		private Rectangle m_collisionBox;

		public GameObject()
        {
			m_sprite = new Sprite();
			Position = new Vector2(0.0f);
			Velocity = new Vector2(0.0f);
			Rotation = 0.0f;
            Scale = 1.0f;
        }

		GameObject(Texture2D p_SpriteTexture)
		{
			m_sprite = new Sprite(p_SpriteTexture);
			Position = new Vector2(0.0f);
			Velocity = new Vector2(0.0f);
			Rotation = 0.0f;
			Scale = 1.0f;
		}

		public void SetTexture(Texture2D p_Texture)
		{
			m_sprite.SetTexture(p_Texture);
		}

        public virtual void Update(GameTime p_GameTime)
        {
            Position += Velocity * p_GameTime.ElapsedGameTime.Milliseconds;
        }

        public virtual void Draw(SpriteBatch p_SpriteBatch)
        {
            m_sprite.Draw(p_SpriteBatch, Position, Rotation, Scale);
        }

		public virtual Rectangle BoundingBox
		{
			get
			{
				m_boundingBox.X = (int)(Position.X - (m_sprite.Origin.X * Scale));
				m_boundingBox.Y = (int)(Position.Y - (m_sprite.Origin.Y * Scale));

				m_boundingBox.Width = (int)(m_sprite.Size.X * Scale);
				m_boundingBox.Height = (int)(m_sprite.Size.Y * Scale);

				return m_boundingBox;
			}
		}

		public virtual Rectangle CollisionBox
		{
			get
			{
				Rectangle boundingBox = BoundingBox;

				m_collisionBox.X = boundingBox.X + CollisionBoundary.X;
				m_collisionBox.Y = boundingBox.Y + CollisionBoundary.Y;

				m_collisionBox.Width = CollisionBoundary.Width;
				m_collisionBox.Height = CollisionBoundary.Height;

				return m_collisionBox;
			}
		}
	}
}
