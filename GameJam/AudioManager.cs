﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;

namespace GameJam
{
    public class AudioManager
    {
        public enum Cue
        {
            CarStart,
            CarIdle,
            CarClimb,
            CarMax,

            Boost,
            MachineGun,
            Rocket,
            Explosion
        };

        public static Dictionary<Cue, SoundEffect> SoundEffects;
        public static Dictionary<Cue, SoundEffectInstance> SoundEffectInstances;

		private static AudioManager instance;
		public static AudioManager Instance
		{
			get
			{
				if (instance == null)
					instance = new AudioManager();

				return instance;
			}
		}

        private AudioManager()
        {
            SoundEffects = new Dictionary<Cue, SoundEffect>
            {
                { Cue.CarStart, Game1.Instance.Content.Load<SoundEffect>("Car_start") },
                { Cue.CarIdle, Game1.Instance.Content.Load<SoundEffect>("Car_idle")  },
                { Cue.CarClimb, Game1.Instance.Content.Load<SoundEffect>("Car_climb")  },
                { Cue.CarMax, Game1.Instance.Content.Load<SoundEffect>("Car_max") },

                { Cue.Boost,  Game1.Instance.Content.Load<SoundEffect>("boost") },
                { Cue.MachineGun,  Game1.Instance.Content.Load<SoundEffect>("machine_gun") },
                { Cue.Rocket,  Game1.Instance.Content.Load<SoundEffect>("rocket") },
                { Cue.Explosion,  Game1.Instance.Content.Load<SoundEffect>("explosion") },
            };

            SoundEffectInstances = new Dictionary<Cue, SoundEffectInstance>
            {
                { Cue.CarStart, SoundEffects[Cue.CarStart].CreateInstance() },
                { Cue.CarIdle, SoundEffects[Cue.CarIdle].CreateInstance() },
                { Cue.CarClimb, SoundEffects[Cue.CarClimb].CreateInstance() },
                { Cue.CarMax, SoundEffects[Cue.CarMax].CreateInstance() },

                { Cue.Boost, SoundEffects[Cue.Boost].CreateInstance() },
                { Cue.MachineGun, SoundEffects[Cue.MachineGun].CreateInstance()  },
                { Cue.Rocket, SoundEffects[Cue.Rocket].CreateInstance()  },
            };
        }

        public static void PauseAll()
        {
            foreach (var item in SoundEffectInstances)
            {
                if (item.Value.State == SoundState.Playing)
                    item.Value.Pause();
            }
        }

        public static void ResumeAll()
        {
            foreach (var item in SoundEffectInstances)
            {
                if (item.Value.State == SoundState.Paused)
                    item.Value.Resume();
            }
        }
    }
}
