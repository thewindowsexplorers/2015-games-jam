﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GameJam.Input
{
    public class PlayerController : ActorController
    {
        GamePadState m_previousState;

        float m_stickThreshold = 0.2f;

        public PlayerController(Vehicle vehicle) : base(vehicle)
        {
            m_previousState = GamePad.GetState(PlayerIndex.One);
        }

        public override void UpdateController(GameTime gameTime)
        {
            GamePadState currentState = GamePad.GetState(PlayerIndex.One);
            //if X pressed then 'new ShootCommand(player)'
            if ((currentState.ThumbSticks.Left.X < -m_stickThreshold) || (currentState.DPad.Left == ButtonState.Pressed)) vehicle.MoveLeft(gameTime);
            if ((currentState.ThumbSticks.Left.X > m_stickThreshold) || (currentState.DPad.Right == ButtonState.Pressed)) vehicle.MoveRight(gameTime);
            if ((currentState.ThumbSticks.Left.Y > m_stickThreshold) || (currentState.DPad.Up == ButtonState.Pressed)) vehicle.MoveUp(gameTime);
            if ((currentState.ThumbSticks.Left.Y < -m_stickThreshold) || (currentState.DPad.Down == ButtonState.Pressed)) vehicle.MoveDown(gameTime);

            if ((currentState.Buttons.A == ButtonState.Released) && (m_previousState.Buttons.A == ButtonState.Pressed)) vehicle.primary.Fire(vehicle.Position,new Vector2(0,-1));//FIRE PRIMARY
            if ((currentState.Buttons.B == ButtonState.Released) && (m_previousState.Buttons.B == ButtonState.Pressed)) ;//FIRE SECONDARY
            if ((currentState.Buttons.X == ButtonState.Released) && (m_previousState.Buttons.X == ButtonState.Pressed)) vehicle.ActivateBoost();
            
            m_previousState = currentState;
        }
    }
}
