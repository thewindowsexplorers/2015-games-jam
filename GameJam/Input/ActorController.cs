﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace GameJam.Input
{
    public abstract class ActorController
    {
         protected Vehicle vehicle;

        public ActorController(Vehicle vehicle)
        {
            this.vehicle = vehicle;
        }

        public abstract void UpdateController(GameTime gameTime);
    }
}
