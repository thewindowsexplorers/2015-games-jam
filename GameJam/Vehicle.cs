﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameJam.Input;
using GameJam.Weapons;

namespace GameJam
{
    public abstract class Vehicle : GameObject
    {
        public ActorController controller { get; set; }
        public int Health { get; private set; }
        public Vector2 MoveSpeed { get; protected set; }

		public PrimaryWeapon primary;

        private float withoutBoostSpeed = 0.0f;
        private float boostSpeed = 0.2f;
        private int boostTime = 2500;
        private int boostElapsed = 0;
        private bool boosting = false;

        protected Vehicle(int startingHealth, Vector2 moveSpeed, Vector2 startingPosition)
        {
            Health = startingHealth;
            MoveSpeed = moveSpeed;
            withoutBoostSpeed = MoveSpeed.X;
            Position = startingPosition;
        }


        public void Damage(int hitPoints)
        {
            Health -= hitPoints;
        }
		
        public void MoveLeft(GameTime gameTime)
        {
            Vector2 pos = Position;
            pos.X -= MoveSpeed.X * gameTime.ElapsedGameTime.Milliseconds;
            Position = pos;
        }

        public void MoveRight(GameTime gameTime)
        {
            Vector2 pos = Position;
            pos.X += MoveSpeed.X * gameTime.ElapsedGameTime.Milliseconds;
            Position = pos;
        }

        public void MoveUp(GameTime gameTime)
        {
            Vector2 pos = Position;
            pos.Y -= MoveSpeed.Y * gameTime.ElapsedGameTime.Milliseconds;
            Position = pos;
        }

        public void MoveDown(GameTime gameTime)
        {
            Vector2 pos = Position;
            pos.Y += MoveSpeed.Y * gameTime.ElapsedGameTime.Milliseconds;
            Position = pos;
        }

        public void ActivateBoost()
        {
            if(boosting == false)
            {
                boosting = true;
                boostElapsed = 0;
                Vector2 speed = MoveSpeed;
                speed.Y = withoutBoostSpeed + boostSpeed;
                MoveSpeed = speed;
            }                       
        }

        private void UpdateBoost(GameTime gameTime)
        {
            boostElapsed += gameTime.ElapsedGameTime.Milliseconds;

            if (boostElapsed >= boostTime)
            {
                boosting = false;
                boostElapsed = 0;
                Vector2 speedIAmSpeed = MoveSpeed;
                speedIAmSpeed.Y = withoutBoostSpeed;
                MoveSpeed = speedIAmSpeed;
            }
        }

        public void Update(GameTime gameTime)
        {
            controller.UpdateController(gameTime);
			UpdateBoost(gameTime);
			base.Update(gameTime);
        }

        
        // Iweapon
		// Icontroller
	}
}
