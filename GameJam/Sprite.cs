﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameJam
{
    public class Sprite
    {
        public Vector2 Size { get; private set; }
        public Vector2 Origin { get; private set; }

		private Texture2D m_texture;

		public Sprite() { }

        public Sprite(Texture2D p_Texture)
        {
			m_texture = p_Texture;
            Origin = new Vector2(m_texture.Width / 2.0f, m_texture.Height / 2.0f);
            Size = new Vector2(m_texture.Width, m_texture.Height);
        }

		public void SetTexture(Texture2D p_Texture)
		{
			m_texture = p_Texture;
			Origin = new Vector2(m_texture.Width / 2.0f, m_texture.Height / 2.0f);
			Size = new Vector2(m_texture.Width, m_texture.Height);
		}

        public virtual void Draw(SpriteBatch p_SpriteBatch, Vector2 p_Position, float p_Rotation, float p_Scale)
        {
            if (m_texture == null)
                return;

            p_SpriteBatch.Draw(m_texture, p_Position, null, null, Origin, p_Rotation, new Vector2(p_Scale, p_Scale), Color.White, SpriteEffects.None, 0);
        }
    }
}
