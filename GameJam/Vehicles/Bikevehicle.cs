﻿using GameJam.Input;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameJam.Vehicles
{
	class BikeVehicle : Vehicle
	{
		public BikeVehicle(Vector2 moveSpeed, Vector2 startingPosition) : base(50, moveSpeed, startingPosition)
		{

		}
	}
}
