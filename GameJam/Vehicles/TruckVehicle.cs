﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using GameJam.Input;

namespace GameJam.Vehicles
{
	class TruckVehicle : Vehicle
	{
		public TruckVehicle(Vector2 moveSpeed, Vector2 startingPosition) : base(1000, moveSpeed, startingPosition)
		{

		}
	}
}
