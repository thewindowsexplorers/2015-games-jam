﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GameJam.Projectiles
{
	public class ProjectilePool
	{
		private static ProjectilePool instance;
		public static ProjectilePool Instance
		{
			get
			{
				if (instance == null)
					instance = new ProjectilePool();

				return instance;
			}
		}

		Projectile[] m_projectiles;
		Texture2D p_bulletTex;
		Texture2D p_rocketTex;

		private ProjectilePool()
		{
			m_projectiles = new Projectile[100]; //Make larger?

			for (int i = 0; i < m_projectiles.Length; i++)
			{
				m_projectiles[i] = new Projectile();
			}
		}

		public void LoadTexture(Game p_game)
		{
			p_bulletTex = p_game.Content.Load<Texture2D>("Bullet");
			p_rocketTex = p_game.Content.Load<Texture2D>("missile");
		}

		public void FireBasicProjectile(Vector2 p_pos, Vector2 p_vel)
		{
			foreach(Projectile p in m_projectiles)
			{
				if(p.IsAlive == false)
				{
					p.Damage = 1;
					p.SetTexture(p_bulletTex);
					p.Fire(p_pos, p_vel);
					break;
				}
			}
		}

		public void FireRocket(Vector2 p_pos, Vector2 p_vel)
		{
			foreach (Projectile p in m_projectiles)
			{
				if (p.IsAlive == false)
				{
					p.Damage = 10;
					p.SetTexture(p_rocketTex);
					p.Fire(p_pos, p_vel);
					break;
				}
			}
		}

		public void Update(GameTime p_GameTime)
		{
			foreach (Projectile p in m_projectiles)
			{
				if (p.IsAlive)
				{
					p.Update(p_GameTime);

					//Check if bullet leaves screen
					if (p.Position.X > 1080 || p.Position.X < 0) //Magic numbers, should get the screen size somehow.
						p.Stop();
					if (p.Position.Y > 1920 || p.Position.Y < 0)
						p.Stop();
				}
			}
		}

		public void Draw(SpriteBatch p_SpriteBatch)
		{
			foreach (Projectile p in m_projectiles)
			{
				if(p.IsAlive)
					p.Draw(p_SpriteBatch);
			}
		}
	}
}
