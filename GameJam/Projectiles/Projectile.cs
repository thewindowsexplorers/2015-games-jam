﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GameJam.Projectiles
{
	class Projectile : GameObject
	{
		//Vector2 m_velocity;
		//Vector2 m_position;
		
		public bool IsAlive { get; private set; }
		public int Damage { get; set; }
		public Texture2D Sprite { private get; set; }

		public Projectile()
		{
			Velocity = new Vector2(0.0f);
			Position = new Vector2(0.0f);

			IsAlive = false;
			Damage = 1;
			Sprite = null;
		}

		public void Fire(Vector2 p_pos, Vector2 p_velocity)
		{
			IsAlive = true;
			Position = p_pos;
			Velocity = p_velocity;
		}

		/// <summary>
		/// Stops and resets the projectile
		/// </summary>
		public void Stop()
		{
			IsAlive = false;
		}
	}
}
