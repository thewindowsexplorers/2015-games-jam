﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using GameJam.Projectiles;

using GameJam.Vehicles;
using GameJam.AIControllers;
using GameJam.Weapons;
using GameJam.Input;
using Microsoft.Xna.Framework.Media;

namespace GameJam
{
    public class Game1 : Game
    {
        private bool inMenu = false;
        public static Game1 Instance;

        //Essential variables
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        RenderTargetScaler _scaler;
		ProjectilePool Projectiles;

        //Game Variables
        ScrollingBackground background;

        //Window Size Definition
        public static int ScreenWidth = 1080;
        public static int ScreenHeight = 1920;

		Vehicle testGO;
		Vehicle player;

		AudioManager audioManger;

        Song music;
        SoundEffectInstance car_start_instance;
        SoundEffectInstance car_idle_instance;
        SoundEffectInstance car_climb_instance;
        SoundEffectInstance car_max_instance;



        //Start menu
        Sprite titleSprite;
        Sprite StartButton;
        Sprite bobCar;




		CarVehicle turret1;
		CarVehicle turret2;
		CarVehicle turret3;
        public Game1()
        {
            Instance = this;

            graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = ScreenWidth,
                PreferredBackBufferHeight = ScreenHeight
            };
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
			audioManger = AudioManager.Instance;
            _scaler = new RenderTargetScaler(this, graphics, ScreenWidth, ScreenHeight);
            background = new ScrollingBackground(this);

			player = new CarVehicle(50,new Vector2(1,1),new Vector2(10,1000));
			player.controller = new PlayerController(player);

			testGO = new CarVehicle(50, new Vector2(0.1f, 0.1f), new Vector2(100, 100));
			Vector2[] points = new Vector2[] { new Vector2(100, 100), new Vector2(500, 1000), new Vector2(100, 1000), new Vector2(500, 100) };

			Texture2D testGOTex = Content.Load<Texture2D>("turrent_centre");
			turret1 = new CarVehicle(50, new Vector2(0.1f, 0.1f), new Vector2(100, 100));
			turret1.controller = new TurretEnemyController(turret1, 1000, player);
			turret1.primary = new BasicGun();
			turret1.SetTexture(testGOTex);

			turret2 = new CarVehicle(50, new Vector2(0.1f, 0.1f), new Vector2(100, 100));
			turret2.controller = new TurretEnemyController(turret2, 2000, player);
			turret2.primary = new BasicGun();
			turret2.SetTexture(testGOTex);

			turret3 = new CarVehicle(50, new Vector2(0.1f, 0.1f), new Vector2(100, 100));
			turret3.controller = new TurretEnemyController(turret3, 3000, player);
			turret3.primary = new BasicGun();
			turret3.SetTexture(testGOTex);
			testGO.controller = new ContainerEnemyController(testGO, new Vector2(500,500),turret1, turret2, turret3);
			Projectiles = ProjectilePool.Instance;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

			Projectiles.LoadTexture(this);

			Texture2D truck = Content.Load<Texture2D>("Enemy Truck");
			testGO.primary = new BasicGun();
			testGO.SetTexture(truck);

			Texture2D testGOTex = Content.Load<Texture2D>("Player Car Single");
			player.primary = new BasicGun();
			player.SetTexture(testGOTex);

            music = Content.Load<Song>("game_music");
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Volume = 0.2f;
            MediaPlayer.Play(music);
			
            //car_start_instance = audioManger.SoundEffects[AudioManager.Cue.CarStart].CreateInstance();
            //car_start_instance.Play();
			//
            //car_idle_instance = audioManger.SoundEffects[AudioManager.Cue.CarIdle].CreateInstance();
            //car_idle_instance.IsLooped = true;
			//
            //car_climb_instance = audioManger.SoundEffects[AudioManager.Cue.CarClimb].CreateInstance();
			//
            //car_max_instance = AudioManager.SoundEffects[AudioManager.Cue.CarMax].CreateInstance();
            //car_max_instance.IsLooped = true;
        }

        protected override void UnloadContent() { }

        protected override void Update(GameTime p_GameTime)
        {
            if (inMenu)
            {
                //start menu logic


            } else
            {
            background.Update(p_GameTime);
			Projectiles.Update(p_GameTime);

			player.Update(p_GameTime);
			testGO.Update(p_GameTime);

            //if (car_start_instance.State != SoundState.Playing)
            //    car_idle_instance.Play();

            //if ((int)p_GameTime.TotalGameTime.TotalSeconds > 5)
            //{
            //    car_idle_instance.Stop();
            //    car_climb_instance.Play();
            //        if (car_climb_instance.State != SoundState.Playing)
            //        car_max_instance.Play();
            //}
            base.Update(p_GameTime);
        }

            
        }

       protected override void Draw(GameTime gameTime)
        {
            _scaler.SetRenderTarget();
            graphics.GraphicsDevice.Clear(Color.CornflowerBlue);

			//Background has it's own spritebatch.Begin() requirements.
            background.Draw(gameTime);

			//Objects that don't need special draw requirements go here.
            spriteBatch.Begin();

            if(inMenu)
            {
                //Start menu draw
            }
            else
            {
			testGO.Draw(spriteBatch);
			turret1.Draw(spriteBatch);
			turret2.Draw(spriteBatch);
			turret3.Draw(spriteBatch);
			player.Draw(spriteBatch);
			Projectiles.Draw(spriteBatch);
            }
			
			

            spriteBatch.End();

            _scaler.Draw();
        }
    }
}
