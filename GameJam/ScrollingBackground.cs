﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameJam
{
    class ScrollingBackground
    {
        private int scrollY;
        private Texture2D texture;
        private SpriteBatch spriteBatch;

        public ScrollingBackground(Game game)
        {
            scrollY = 0;
            texture = game.Content.Load<Texture2D>("background");
            spriteBatch = new SpriteBatch(game.GraphicsDevice);
        }

        public void Update(GameTime gameTime)
        {
            scrollY -= gameTime.ElapsedGameTime.Milliseconds;
        }

        public void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.LinearWrap, null, null, null, null);
            Rectangle source = new Rectangle(0, scrollY, texture.Width, texture.Height);
            spriteBatch.Draw(texture, new Rectangle(0, 0, texture.Width, texture.Height), source, Color.White);
            spriteBatch.End();
        }
    }
}
