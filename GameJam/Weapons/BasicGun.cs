﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameJam.Weapons
{
	class BasicGun : PrimaryWeapon
	{
		public BasicGun()
		{

		}

		public override void Fire(Vector2 p_pos, Vector2 p_vel)
		{
			Projectiles.FireBasicProjectile(p_pos, p_vel);
		}
	}
}
