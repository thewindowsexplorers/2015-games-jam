﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameJam.Projectiles;
using Microsoft.Xna.Framework;

namespace GameJam.Weapons
{
	 public abstract class PrimaryWeapon : IWeapon
	{
		protected ProjectilePool Projectiles;

		//Fires basic projectiles
		public PrimaryWeapon()
		{
			Projectiles = ProjectilePool.Instance;
		}

		abstract public void Fire(Vector2 p_pos, Vector2 p_vel);
	}
}
