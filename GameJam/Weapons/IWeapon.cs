﻿using Microsoft.Xna.Framework;

namespace GameJam.Weapons
{
	interface IWeapon
	{
		/// <summary>
		/// Fires a projectile
		/// </summary>
		void Fire(Vector2 p_pos, Vector2 p_vel);
	}
}
