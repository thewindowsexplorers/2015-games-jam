﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace GameJam.Weapons
{
	class RocketLauncher : SecondaryWeapon
	{
		public RocketLauncher()
		{

		}

		public override void Fire(Vector2 p_pos, Vector2 p_vel)
		{
			Projectiles.FireRocket(p_pos, p_vel);
		}
	}
}
