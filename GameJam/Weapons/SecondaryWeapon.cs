﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using GameJam.Projectiles;

namespace GameJam.Weapons
{
	abstract class SecondaryWeapon : IWeapon
	{
		protected ProjectilePool Projectiles;

		public SecondaryWeapon()
		{
			Projectiles = ProjectilePool.Instance;
		}

		abstract public void Fire(Vector2 p_pos, Vector2 p_vel);
	}
}
